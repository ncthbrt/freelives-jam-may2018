FROM node:9.11.1
COPY . . 
RUN yarn install
RUN yarn now-build
EXPOSE 3000
CMD [ "yarn", "run", "start" ]