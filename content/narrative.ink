VAR company_name = "Fixify"
VAR employee_name = "@AliceCryzstal"
VAR doom = 0
VAR trust_in_you = 0.5
VAR popularity = 0.5
LIST industries = healthcare, transport, dating, food
VAR industry = healthcare
VAR current_project="Quantas Project"
<MikeFirth> MikeFirth: Wecome {employee_name}! We're glad that you made the decision to join {company_name}.
<MikeFirth> MikeFirth: We're on a mission to <>
* <MikeFirth> MikeFirth: revolutionise the health care industry 
    -> mission_revolutionise_health_care 
* <MikeFirth> MikeFirth: solve transport
    -> mission_improve_public_transport 
* <MikeFirth> MikeFirth: fix dating
    -> mission_fix_dating
* <MikeFirth> MikeFirth: make food great again
    -> mission_fast_food

== mission_revolutionise_health_care ==
~ industry = healthcare
~ doom = 0.2
<MikeFirth> MikeFirth: We're doing this through the strategic application of nano-imputers, allowing precise control of hormonal and micro-nutrient balances.
-> onboarding

== mission_improve_public_transport ==
~ industry = transport
~ doom = 0.4
<MikeFirth> MikeFirth: We aim to achieve this by providing tools to connect people, helping them to get where they need to go.
* <MikeFirth> You: So like Uber?
    <MikeFirth> MikeFirth: Definitely not. Our business model is far more disruptive! Uber just is cross-subsiding rides with billions of investor dollars.
    ** <MikeFirth> You: 👍🙂 Makes sense.Always been scehptical about uber. Keen to get going!
    -> onboarding
* <MikeFirth> You: 👍🙂 Keen to get going!
-
-> onboarding

== mission_fix_dating == 
~ industry = dating
~ doom = 0.2
<> through sophisticated matching AI and a feature which records consent on the blockchain.
-> onboarding

== mission_fast_food == 
~ doom = 0.3
~ industry = food
<> through the use of robotic food dispenseries. Our signature tech is not only efficient, but ethical; it helps late night snackers through the use of critical behavioural enforcement.
-> onboarding

== onboarding == 
<MikeFirth> MikeFirth: Anyways @MikeScott (we're a two mike company 😂), will get you onboarded. Cheerio. Got some meetings to do.
* <MikeFirth> You: Cheerio. 🤠
-
<MikeScott> MikeScott: Hi {employee_name}. Mike here (think you've met the other one, our glorious leader 🙄😜). I'm head of the dev team
<MikeScott> MikeScott: Friend who recommended you said you're a real whiz on the backend. So I'm going to put you on {current_project}.
<MikeScott> MikeScott: Think you'll be a great help. I'm sure you're already familiar with our stack: NeedleLang, OstrichDb and OverReact.js.
* <MikeScott> You: Yes. Been using that stack for five years.
    ~ trust_in_you += 0.1
    <MikeScott> MikeScott: 👍🙂 Excellent!
* <MikeScott> You: No. Think that OverReact was only released two weeks ago
    ~ trust_in_you -= 0.1
    <MikeScott> MikeScott: Don't worry. I'm sure you'll pick it up soon enough
-
<MikeScott> MikeScott: Just need to setup you up with gitcentre credentials. Will ping you when ready. In the meantime, you should say hi to the rest of the team in \#general.
* <MikeScott> You: Thanks @MikeScott.
<MikeScott> MikeScott: No problem. 👌
-
* <general> You: Hi @everyone. I'm {employee_name}.
-
~ temp waved_to_cris = false
<general> CrisElk: Hi {employee_name}. I'm Cris. Experience Design team. We will be working on the same project. Hope we can push some kick ass product toghether.
* <general> [\*react with 👋*] You: 👋
    ~popularity +=0.1
    ~ waved_to_cris = true
* <general> [\*do nothing*]
    ~ waved_to_cris = false
-
<general> ElizabethStern: Hi {employee_name}. Good to get another person who isn't a pale male on the team.  
<general> CrisElk: Hey! What's that supposed to mean? 
<general> ElizabethStern: Nothing @CrisElk. Absolutely nothing.
<general> ElizabethStern: Moving on, I'm the frontend dev on your project. So I will one of the people shouting at you if our servers topple over 😜🙈
<general> DonLennox: Hi {employee_name} I'm Don (but everone else calls me Marvin for some reason). I'm the other backend dev on {current_project}.
<ElizabethStern> ElizabethStern: Hey. Really am glad you're here. 
{ waved_to_cris: 
    <CrisElk> CrisElk: Hi. Glad to have you onboard. Hoping your XP will rub off on some of the more junior devs. They aren't always the most professional.
    * <CrisElk> You: I'll try my best @CrisElk. 
    -> stern_convo
    * <CrisElk> You: If you don't mind me saying, but talking to someone about colleagues on the first day doesn't seem that professional either.
        <CrisElk> CrisElk: You may be right. Nevertheless it's true
        ** <CrisElk> You: Ok. Thanks for the warning.
        -> stern_convo
    * <CrisElk> [*ask about @ElizabethStern*] You: Are you referring to @ElizabethStern? 
        <CrisElk> CrisElk: Good guess. Sometimes quite difficult to get her to propery implement my designs. Would advise you to keep your eyes on her.
        ** <CrisElk> You: Hmmm. I'll keep an eye out. 
            <CrisElk> CrisElk: Thanks
            -> stern_convo
        ** <CrisElk> You: Wonder why? 
            <CrisElk> CrisElk: ¯\\_(ツ)_/¯ No clue. It's a little infuriating.
            -> stern_convo
        
        
- else:
    <general> ElizabethStern: You still haven't read The Hitchhiker's guide, have you @DonLennox?
    -> stern_convo
}
= stern_convo
<ElizabethStern> ElizabethStern: You get some true bozos like CrisElk in this company. Hope you'll balance things out a bit.
* <general> [*ignore ElizabethStern*] Hi everyone
* <ElizabethStern> [*nervously appreciative*]  You: Thanks for the warm welcome. Pretty nervous about this job. Haven't worked in a startup before. 
<ElizabethStern> ElizabethStern: No problem. You'll do fine! You've had plenty of other XP. Will chat more later.
* <ElizabethStern> [*continue conversation in general*] You: Thanks. As it's my first day here, hope you don't mind if I reserve judgement about colleagues till I get to know them. 
    <ElizabethStern> ElizabethStern: Alright. You'll see what I mean later on tho.
* <ElizabethStern> [*ask about tension between @CrisElk and @ElizabethStern*] You: Feels like there is a little bit of tension than is usual between you and Cris? 
    <ElizabethStern> ElizabethStern: Who between me and Cris? Phhhh. Think it's just that he can't handle a frontend developer who is willing to push back on his inane pixel pushing bs. 
    ** <ElizabethStern> [*shocked*] You: Wow. Don't know what to say to that. Haha. Barely said hello to anyone. Already getting all the secrets
    <ElizabethStern> ElizabethStern: There are far more secrets than just that, my friend. *Plays dramatic music* 🧛️
        *** <ElizabethStern> You: Haha 😂
        <ElizabethStern> ElizabethStern: 😀 anyway lets get back to \#general
        *** <ElizabethStern> You: ?
        <ElizabethStern> ElizabethStern: Just a joke. Anyway lets get back to \#general
    ** <ElizabethStern> [*symathetic*]  You: I understand. Saw that happen to a dev at my previous job. Dealing with it made him a very angry person. 
        <ElizabethStern> ElizabethStern: 👍 Wouldn't say I'm exactly an angry person, but that kind of mindless pretension thing just gets to me. I sometimes think he thinks he is the ghost of  Jobs. Anyways lets get back to \#general.
-
* <general> [*ask to be shown around the backend codebase*] You: Thanks in advance for getting me set up and going @everyone. @MikeScott said he'd set my up with GitCentre credentials. @DonLennox mind setting up a vid call so that you can show me around the code base plz?
    <general> DonLennox: Sure thing. Will send you an invite to Skaap. 
* <general> [*ask to be shown the frontend by @CrisElk*] You: @CrisElk mind showing me the product we're actually working on? Would help contextualize the backend requirements. 
    <general> CrisElk: Sure thing. I'd be happy to.
* <general> [*ask to be shown the frontend by @ElizabethStern*] You: @ElizabethStern mind showing me around the frontend, would like to see what we're working on. 
    <general> ElizabethStern: Of course. Will set up a Skaap call.
-
<general> FIN.
-> END 


