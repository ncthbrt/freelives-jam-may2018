'use strict';

var Block = require("bs-platform/lib/js/block.js");
var Curry = require("bs-platform/lib/js/curry.js");
var Inkjs = require("inkjs");
var React = require("react");
var Random = require("bs-platform/lib/js/random.js");
var Caml_obj = require("bs-platform/lib/js/caml_obj.js");
var Belt_List = require("bs-platform/lib/js/belt_List.js");
var Belt_Array = require("bs-platform/lib/js/belt_Array.js");
var Belt_Option = require("bs-platform/lib/js/belt_Option.js");
var ReasonReact = require("reason-react/src/ReasonReact.js");
var Js_primitive = require("bs-platform/lib/js/js_primitive.js");
var Caml_builtin_exceptions = require("bs-platform/lib/js/caml_builtin_exceptions.js");

function delay(ms) {
  return new Promise((function (resolve, _) {
                setTimeout((function () {
                        return resolve(/* () */0);
                      }), ms);
                return /* () */0;
              }));
}

function getState(story) {
  return story.state.toJson();
}

function loadState(story, json) {
  var state = story.state;
  state.LoadJson(json);
  return story;
}

function fromString(str) {
  return new Inkjs.Story(str);
}

function continueMaximallyWithMap_(_lst, story, f) {
  while(true) {
    var lst = _lst;
    if (story.canContinue) {
      _lst = /* :: */[
        Curry._2(f, story, story.Continue()),
        lst
      ];
      continue ;
    } else {
      return lst;
    }
  };
}

function continueMaximallyWithMap(story, f) {
  return continueMaximallyWithMap_(/* [] */0, story, f);
}

function currentChoices(story) {
  return Belt_List.map(Belt_List.fromArray(story.currentChoices), (function (c) {
                return /* record */[
                        /* choicePoint */c.choicePoint,
                        /* threadAtGeneration */c.threadAtGeneration,
                        /* text */c.text,
                        /* index */c.index
                      ];
              }));
}

function choose(s, choice) {
  s.ChooseChoiceIndex(choice[/* index */3]);
  return s;
}

function continueMaximally(story) {
  return continueMaximallyWithMap_(/* [] */0, story, (function (_, s) {
                return s;
              }));
}

function continueMaximallyWithTags(story) {
  return continueMaximallyWithMap_(/* [] */0, story, (function (story, s) {
                return /* tuple */[
                        s,
                        story.currentTags
                      ];
              }));
}

function Story_003(prim) {
  return prim.canContinue;
}

function Story_004(prim) {
  return prim.Continue();
}

function Story_010(prim) {
  return prim.currentTags;
}

var Story = /* module */[
  /* getState */getState,
  /* loadState */loadState,
  /* fromString */fromString,
  Story_003,
  Story_004,
  /* continueMaximally */continueMaximally,
  /* continueMaximallyWithTags */continueMaximallyWithTags,
  /* continueMaximallyWithMap */continueMaximallyWithMap,
  /* currentChoices */currentChoices,
  /* choose */choose,
  Story_010
];

var scrollToBottom = (
  function scrollToBottom() {
    var progress = 0.0;
    var start = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
    var dist = document.body.scrollHeight - window.innerHeight - start;
    if( dist < 0 ) return;

    var duration = 300 + 300*dist/100;
    var startTime = null;
    function step(time) {
        if( startTime == null ) startTime = time;
        var t = (time-startTime) / duration;
        var lerp = 3*t*t - 2*t*t*t;
        window.scrollTo(0, start + lerp*dist);
        if( t < 1 ) requestAnimationFrame(step);
    }
    requestAnimationFrame(step);
});

function continueMaximallyWithAssocTags(story) {
  var aux = function (s) {
    var match = Belt_List.fromArray(s.split(":"));
    if (match) {
      var values = match[1];
      var key = match[0];
      if (values) {
        return /* tuple */[
                key,
                /* Some */[Belt_List.toArray(values).join(":").trim()]
              ];
      } else {
        return /* tuple */[
                key,
                /* None */0
              ];
      }
    } else {
      return /* tuple */[
              "",
              /* None */0
            ];
    }
  };
  return continueMaximallyWithMap_(/* [] */0, story, (function (story, text) {
                return /* tuple */[
                        text,
                        Belt_List.fromArray(Belt_Array.map(Curry._1(Story_010, story), aux))
                      ];
              }));
}

var component = ReasonReact.reducerComponent("Story");

var channelRegex = (/^(?:\s*<)(.*)(?:>)(.*)$/m);

function getChannelAndText(str) {
  var matches = Belt_Option.getWithDefault(Belt_Option.map(Js_primitive.null_to_opt(channelRegex.exec(str)), (function (prim) {
              return prim;
            })), /* array */[]);
  if (matches.length !== 3) {
    throw [
          Caml_builtin_exceptions.failure,
          "No matches for " + str
        ];
  } else {
    var channel = matches[1];
    var txt = matches[2];
    return /* tuple */[
            Belt_Option.getExn((channel == null) ? /* None */0 : [channel]).trim(),
            Belt_Option.getExn((txt == null) ? /* None */0 : [txt]).trim()
          ];
  }
}

var privateChannels = /* array */[
    "MikeScott",
    "MikeFirth",
    "CrisElk",
    "ElizabethStern",
    "DonKnox"
  ].sort();

var publicChannels = /* array */[
    "general",
    "random",
    "standup"
  ].sort();

function hasNextDialog(channel, newDialog, seen) {
  if (Belt_List.has(newDialog, channel, (function (dialog, channel) {
            return getChannelAndText(dialog)[0] === channel;
          }))) {
    return !Belt_List.has(seen, channel, Caml_obj.caml_equal);
  } else {
    return false;
  }
}

function make() {
  return /* record */[
          /* debugName */component[/* debugName */0],
          /* reactClassInternal */component[/* reactClassInternal */1],
          /* handedOffState */component[/* handedOffState */2],
          /* willReceiveProps */component[/* willReceiveProps */3],
          /* didMount */(function (self) {
              return Curry._1(self[/* send */3], /* FetchStory */0);
            }),
          /* didUpdate */component[/* didUpdate */5],
          /* willUnmount */component[/* willUnmount */6],
          /* willUpdate */component[/* willUpdate */7],
          /* shouldUpdate */component[/* shouldUpdate */8],
          /* render */(function (self) {
              var match = self[/* state */1][/* loadingState */0];
              switch (match) {
                case 0 : 
                    return React.createElement("div", undefined, "Loading...");
                case 1 : 
                    return React.createElement("div", undefined, "An error occurred!");
                case 2 : 
                    return React.createElement("div", {
                                className: "sleck"
                              }, React.createElement("div", {
                                    className: "sidebars-sidebar"
                                  }, React.createElement("div", {
                                        className: "sidebars-sidebar-workspace"
                                      }, React.createElement("img", {
                                            src: "fixify.png"
                                          }))), React.createElement("div", {
                                    className: "sidebar"
                                  }, React.createElement("h2", {
                                        className: "sidebar-content"
                                      }, "Fixify ▾"), React.createElement("p", {
                                        className: "sidebar-content"
                                      }, "☆ AliceCryzstal"), React.createElement("div", {
                                        className: "public-channels"
                                      }, React.createElement("h4", {
                                            className: "sidebar-content"
                                          }, "Channels"), Belt_Array.map(publicChannels, (function (channel) {
                                              var match = self[/* state */1][/* currentChannel */5] === channel;
                                              var match$1 = hasNextDialog(channel, self[/* state */1][/* newDialogue */4], self[/* state */1][/* seen */6]);
                                              return React.createElement("div", {
                                                          key: channel,
                                                          className: " channel " + (
                                                            match ? " channel-current " : ""
                                                          ),
                                                          onClick: (function () {
                                                              return Curry._1(self[/* send */3], /* OpenChannel */Block.__(2, [channel]));
                                                            })
                                                        }, React.createElement("span", undefined, match$1 ? "✳️ " : " "), React.createElement("span", undefined, "# " + channel));
                                            }))), React.createElement("div", {
                                        className: "private-channels"
                                      }, React.createElement("h4", {
                                            className: "sidebar-content"
                                          }, "Direct Messages"), Belt_Array.map(privateChannels, (function (channel) {
                                              var match = self[/* state */1][/* currentChannel */5] === channel;
                                              var match$1 = hasNextDialog(channel, self[/* state */1][/* newDialogue */4], self[/* state */1][/* seen */6]);
                                              return React.createElement("div", {
                                                          key: channel,
                                                          className: "channel " + (
                                                            match ? " channel-current " : ""
                                                          ),
                                                          onClick: (function () {
                                                              return Curry._1(self[/* send */3], /* OpenChannel */Block.__(2, [channel]));
                                                            })
                                                        }, React.createElement("span", undefined, match$1 ? "✳️ " : " "), React.createElement("span", undefined, " " + channel));
                                            })))), React.createElement("div", {
                                    className: "chat-header"
                                  }, React.createElement("h4", undefined, self[/* state */1][/* currentChannel */5])), React.createElement("div", {
                                    className: "chat-window"
                                  }, React.createElement("div", {
                                        className: "chat-history-container"
                                      }, React.createElement("div", {
                                            className: "chat-history"
                                          }, Belt_List.toArray(Belt_List.reverse(Belt_List.mapWithIndex(Belt_List.keep(Belt_List.map(self[/* state */1][/* dialogue */3], getChannelAndText), (function (param) {
                                                              return param[0] === self[/* state */1][/* currentChannel */5];
                                                            })), (function (i, param) {
                                                          return React.createElement("p", {
                                                                      key: String(i)
                                                                    }, param[1]);
                                                        }))))))), React.createElement("div", {
                                    className: "chat-options"
                                  }, Belt_List.toArray(Belt_List.map(Belt_List.keep(Belt_List.map(self[/* state */1][/* choices */2], (function (choice) {
                                                      return /* tuple */[
                                                              choice,
                                                              getChannelAndText(choice[/* text */2])
                                                            ];
                                                    })), (function (param) {
                                                  return param[1][0] === self[/* state */1][/* currentChannel */5];
                                                })), (function (a) {
                                              var choice = a[0];
                                              console.log(a);
                                              return React.createElement("div", {
                                                          key: String(choice[/* choicePoint */0])
                                                        }, React.createElement("button", {
                                                              className: "choice",
                                                              href: "#",
                                                              onClick: (function () {
                                                                  return Curry._1(self[/* send */3], /* MakeChoice */Block.__(1, [choice]));
                                                                })
                                                            }, a[1][1]));
                                            })))));
                
              }
            }),
          /* initialState */(function () {
              return /* record */[
                      /* loadingState : Loading */0,
                      /* story : None */0,
                      /* choices : [] */0,
                      /* dialogue : [] */0,
                      /* newDialogue : [] */0,
                      /* currentChannel */"MikeFirth",
                      /* seen : :: */[
                        "MikeFirth",
                        /* [] */0
                      ]
                    ];
            }),
          /* retainedProps */component[/* retainedProps */11],
          /* reducer */(function (action, _state) {
              if (typeof action === "number") {
                if (action === 0) {
                  return /* UpdateWithSideEffects */Block.__(2, [
                            _state,
                            (function (self) {
                                fetch("narrative.ink.json").then((function (r) {
                                            var match = r.status === 200;
                                            if (match) {
                                              return r.text();
                                            } else {
                                              return Promise.reject(Caml_builtin_exceptions.not_found);
                                            }
                                          })).then((function (story) {
                                          return Promise.resolve(Curry._1(self[/* send */3], /* StoryFetched */Block.__(0, [new Inkjs.Story(story)])));
                                        })).catch((function (_err) {
                                        console.error(_err);
                                        return Promise.resolve(Curry._1(self[/* send */3], /* StoryFailedToFetch */1));
                                      }));
                                return /* () */0;
                              })
                          ]);
                } else {
                  return /* Update */Block.__(0, [/* record */[
                              /* loadingState : Error */1,
                              /* story */_state[/* story */1],
                              /* choices */_state[/* choices */2],
                              /* dialogue */_state[/* dialogue */3],
                              /* newDialogue */_state[/* newDialogue */4],
                              /* currentChannel */_state[/* currentChannel */5],
                              /* seen */_state[/* seen */6]
                            ]]);
                }
              } else {
                switch (action.tag | 0) {
                  case 0 : 
                      var story = action[0];
                      var dialogue = continueMaximallyWithMap_(/* [] */0, story, (function (_, s) {
                              return s;
                            }));
                      var choices = currentChoices(story);
                      return /* Update */Block.__(0, [/* record */[
                                  /* loadingState : Loaded */2,
                                  /* story : Some */[story],
                                  /* choices */choices,
                                  /* dialogue */dialogue,
                                  /* newDialogue */_state[/* newDialogue */4],
                                  /* currentChannel */_state[/* currentChannel */5],
                                  /* seen */_state[/* seen */6]
                                ]]);
                  case 1 : 
                      var story$1 = Belt_Option.getExn(_state[/* story */1]);
                      var story$2 = choose(story$1, action[0]);
                      var dialog = continueMaximallyWithMap_(/* [] */0, story$2, (function (_, s) {
                              return s;
                            }));
                      var choices$1 = currentChoices(story$2);
                      var nextState_000 = /* loadingState */_state[/* loadingState */0];
                      var nextState_001 = /* story : Some */[story$2];
                      var nextState_003 = /* dialogue */_state[/* dialogue */3];
                      var nextState_005 = /* currentChannel */_state[/* currentChannel */5];
                      var nextState_006 = /* seen : :: */[
                        _state[/* currentChannel */5],
                        /* [] */0
                      ];
                      var nextState = /* record */[
                        nextState_000,
                        nextState_001,
                        /* choices : [] */0,
                        nextState_003,
                        /* newDialogue : [] */0,
                        nextState_005,
                        nextState_006
                      ];
                      return /* UpdateWithSideEffects */Block.__(2, [
                                nextState,
                                (function (self) {
                                    Promise.all(Belt_List.toArray(Belt_List.reduce(Belt_List.reverse(dialog), /* tuple */[
                                                      /* [] */0,
                                                      0
                                                    ], (function (param, text) {
                                                        var time = (param[1] + 400 | 0) + Random.$$int(1000) | 0;
                                                        return /* tuple */[
                                                                /* :: */[
                                                                  delay(time).then((function () {
                                                                          return Promise.resolve(Curry._1(self[/* send */3], /* DialogOptionAdded */Block.__(3, [text])));
                                                                        })),
                                                                  param[0]
                                                                ],
                                                                time
                                                              ];
                                                      }))[0])).then((function () {
                                            return Promise.resolve(Curry._1(self[/* send */3], /* ChoicesAdded */Block.__(4, [choices$1])));
                                          }));
                                    return /* () */0;
                                  })
                              ]);
                  case 2 : 
                      var channel = action[0];
                      return /* Update */Block.__(0, [/* record */[
                                  /* loadingState */_state[/* loadingState */0],
                                  /* story */_state[/* story */1],
                                  /* choices */_state[/* choices */2],
                                  /* dialogue */_state[/* dialogue */3],
                                  /* newDialogue */_state[/* newDialogue */4],
                                  /* currentChannel */channel,
                                  /* seen : :: */[
                                    channel,
                                    _state[/* seen */6]
                                  ]
                                ]]);
                  case 3 : 
                      var text = action[0];
                      return /* Update */Block.__(0, [/* record */[
                                  /* loadingState */_state[/* loadingState */0],
                                  /* story */_state[/* story */1],
                                  /* choices */_state[/* choices */2],
                                  /* dialogue : :: */[
                                    text,
                                    _state[/* dialogue */3]
                                  ],
                                  /* newDialogue : :: */[
                                    text,
                                    _state[/* newDialogue */4]
                                  ],
                                  /* currentChannel */_state[/* currentChannel */5],
                                  /* seen */_state[/* seen */6]
                                ]]);
                  case 4 : 
                      return /* Update */Block.__(0, [/* record */[
                                  /* loadingState */_state[/* loadingState */0],
                                  /* story */_state[/* story */1],
                                  /* choices */action[0],
                                  /* dialogue */_state[/* dialogue */3],
                                  /* newDialogue */_state[/* newDialogue */4],
                                  /* currentChannel */_state[/* currentChannel */5],
                                  /* seen */_state[/* seen */6]
                                ]]);
                  
                }
              }
            }),
          /* subscriptions */component[/* subscriptions */13],
          /* jsElementWrapped */component[/* jsElementWrapped */14]
        ];
}

var R = 0;

exports.R = R;
exports.delay = delay;
exports.Story = Story;
exports.scrollToBottom = scrollToBottom;
exports.continueMaximallyWithAssocTags = continueMaximallyWithAssocTags;
exports.component = component;
exports.channelRegex = channelRegex;
exports.getChannelAndText = getChannelAndText;
exports.privateChannels = privateChannels;
exports.publicChannels = publicChannels;
exports.hasNextDialog = hasNextDialog;
exports.make = make;
/* scrollToBottom Not a pure module */
