module R = ReasonReact;

let delay: int => Js.Promise.t(unit) =
  ms =>
    Js.Promise.make((~resolve, ~reject as _) =>
      Js.Global.setTimeout(() => resolve(. (): unit), ms) |> ignore
    );

module Story: {
  type t;
  type choice = {
    choicePoint: int,
    threadAtGeneration: string,
    text: string,
    index: int,
  };
  let getState: t => Js.Json.t;
  let loadState: (t, Js.Json.t) => t;
  let fromString: string => t;
  let canContinue: t => bool;
  let continue: t => string;
  let continueMaximally: t => list(string);
  let continueMaximallyWithTags: t => list((string, array(string)));
  let continueMaximallyWithMap: (t, (t, string) => 'a) => list('a);
  let currentChoices: t => list(choice);
  let choose: (t, choice) => t;
  let tags: t => array(string);
} = {
  type choice = {
    choicePoint: int,
    threadAtGeneration: string,
    text: string,
    index: int,
  };
  type jsChoice = {
    .
    "choicePoint": int,
    "threadAtGeneration": string,
    "text": string,
    "index": int,
  };
  type t;
  type state;
  [@bs.module "inkjs"] [@bs.new] external create : string => t = "Story";
  [@bs.get] external getState_ : t => state = "state";
  [@bs.send] external loadJson_ : (state, Js.Json.t) => unit = "LoadJson";
  [@bs.send] external toJson : state => Js.Json.t = "toJson";
  let getState = story => {
    let state = getState_(story);
    toJson(state);
  };
  let loadState = (story, json) => {
    let state = getState_(story);
    loadJson_(state, json);
    story;
  };
  let fromString: string => t = str => create(str);
  [@bs.get] external canContinue : t => bool = "";
  [@bs.send] external continue : t => string = "Continue";
  let rec continueMaximallyWithMap_ = (lst, story, f) =>
    if (canContinue(story)) {
      continueMaximallyWithMap_(
        [f(story, continue(story)), ...lst],
        story,
        f,
      );
    } else {
      lst;
    };
  let continueMaximallyWithMap = (story, f) =>
    continueMaximallyWithMap_([], story, f);
  [@bs.get]
  external currentChoices_ : t => array(jsChoice) = "currentChoices";
  let currentChoices = story =>
    Belt.List.fromArray(currentChoices_(story))
    |. Belt.List.map(c =>
         {
           choicePoint: c##choicePoint,
           threadAtGeneration: c##threadAtGeneration,
           text: c##text,
           index: c##index,
         }
       );
  [@bs.send]
  external chooseChoiceIndex_ : (t, int) => unit = "ChooseChoiceIndex";
  let choose = (s, choice) => {
    chooseChoiceIndex_(s, choice.index);
    s;
  };
  let continueMaximally = story =>
    continueMaximallyWithMap(story, (_, s) => s);
  [@bs.get] external tags : t => array(string) = "currentTags";
  let continueMaximallyWithTags = story =>
    continueMaximallyWithMap(story, (story, s) => (s, tags(story)));
};

type loadingState =
  | Loading
  | Error
  | Loaded;

type state = {
  loadingState,
  story: option(Story.t),
  choices: list(Story.choice),
  dialogue: list(string),
  newDialogue: list(string),
  currentChannel: string,
  seen: list(string),
};

type action =
  | FetchStory
  | StoryFetched(Story.t)
  | StoryFailedToFetch
  | MakeChoice(Story.choice)
  | OpenChannel(string)
  | DialogOptionAdded(string)
  | ChoicesAdded(list(Story.choice));

let scrollToBottom: unit => unit = [%bs.raw
  {|
  function scrollToBottom() {
    var progress = 0.0;
    var start = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
    var dist = document.body.scrollHeight - window.innerHeight - start;
    if( dist < 0 ) return;

    var duration = 300 + 300*dist/100;
    var startTime = null;
    function step(time) {
        if( startTime == null ) startTime = time;
        var t = (time-startTime) / duration;
        var lerp = 3*t*t - 2*t*t*t;
        window.scrollTo(0, start + lerp*dist);
        if( t < 1 ) requestAnimationFrame(step);
    }
    requestAnimationFrame(step);
}|}
];

let continueMaximallyWithAssocTags = story => {
  let aux = s =>
    switch (Js.String.split(":", s) |. Belt.List.fromArray) {
    | [key] => (key, None)
    | [key, ...values] => (
        key,
        Some(
          Js.String.trim(Js.Array.joinWith(":", Belt.List.toArray(values))),
        ),
      )
    | [] => ("", None)
    };
  Story.continueMaximallyWithMap(story, (story, text) =>
    (text, Story.tags(story) |. Belt.Array.map(aux) |. Belt.List.fromArray)
  );
};

let component = R.reducerComponent("Story");

let channelRegex = [%bs.re "/^(?:\\s*<)(.*)(?:>)(.*)$/m"];

let getChannelAndText = str => {
  let matches =
    Js.Re.exec(str, channelRegex)
    |. Belt.Option.map(Js.Re.captures)
    |. Belt.Option.getWithDefault([||]);
  switch (matches) {
  | [|_, channel, txt|] => (
      Js.String.trim(channel |. Js.Nullable.toOption |. Belt.Option.getExn),
      Js.String.trim(txt |. Js.Nullable.toOption |. Belt.Option.getExn),
    )
  | _ => raise(Failure("No matches for " ++ str))
  };
};

let privateChannels =
  [|"MikeScott", "MikeFirth", "CrisElk", "ElizabethStern", "DonKnox"|]
  |. Js.Array.sortInPlace;

let publicChannels =
  [|"general", "random", "standup"|] |. Js.Array.sortInPlace;

let hasNextDialog = (channel, newDialog, seen) =>
  Belt.List.has(newDialog, channel, (dialog, channel) =>
    getChannelAndText(dialog) |. fst == channel
  )
  && ! Belt.List.has(seen, channel, (==));

let make = _children => {
  ...component,
  initialState: _state => {
    loadingState: Loading,
    story: None,
    choices: [],
    dialogue: [],
    newDialogue: [],
    currentChannel: "MikeFirth",
    seen: ["MikeFirth"],
  },
  reducer: (action, _state) =>
    switch (action) {
    | FetchStory =>
      R.UpdateWithSideEffects(
        _state,
        (
          self =>
            Js.Promise.(
              Fetch.fetch("narrative.ink.json")
              |> then_(r =>
                   Fetch.Response.status(r) == 200 ?
                     Fetch.Response.text(r) : reject(Not_found)
                 )
              |> then_(story =>
                   resolve(
                     self.send(StoryFetched(Story.fromString(story))),
                   )
                 )
              |> catch(_err => {
                   Js.Console.error(_err);
                   resolve(self.send(StoryFailedToFetch));
                 })
              |> ignore
            )
        ),
      )
    | StoryFetched(story) =>
      let dialogue = Story.continueMaximally(story);
      let choices = Story.currentChoices(story);
      R.Update({
        ..._state,
        story: Some(story),
        dialogue,
        choices,
        loadingState: Loaded,
      });
    | StoryFailedToFetch => R.Update({..._state, loadingState: Error})
    | MakeChoice(choice) =>
      let story = Belt.Option.getExn(_state.story);
      let story = Story.choose(story, choice);
      let dialog = Story.continueMaximally(story);
      let choices = Story.currentChoices(story);
      let nextState = {
        ..._state,
        story: Some(story),
        newDialogue: [],
        choices: [],
        seen: [_state.currentChannel],
      };
      R.UpdateWithSideEffects(
        nextState,
        (
          self =>
            Js.Promise.(
              all(
                dialog
                |. Belt.List.reverse
                |. Belt.List.reduce(
                     ([], 0),
                     ((prev, time), text) => {
                       let time = time + 400 + Random.int(1000);
                       (
                         [
                           delay(time)
                           |> then_(() =>
                                Js.Promise.resolve(
                                  self.send(DialogOptionAdded(text)),
                                )
                              ),
                           ...prev,
                         ],
                         time,
                       );
                     },
                   )
                |. fst
                |. Belt.List.toArray,
              )
              |> then_((_) =>
                   Js.Promise.resolve(self.send(ChoicesAdded(choices)))
                 )
              |> ignore
            )
        ),
      );
    | OpenChannel(channel) =>
      R.Update({
        ..._state,
        currentChannel: channel,
        seen: [channel, ..._state.seen],
      })
    | DialogOptionAdded(text) =>
      R.Update({
        ..._state,
        newDialogue: [text, ..._state.newDialogue],
        dialogue: [text, ..._state.dialogue],
      })
    | ChoicesAdded(choices) => R.Update({..._state, choices})
    },
  didMount: self => self.send(FetchStory),
  render: self =>
    switch (self.state.loadingState) {
    | Error => <div> (R.string("An error occurred!")) </div>
    | Loading => <div> (R.string("Loading...")) </div>
    | Loaded =>
      <div className="sleck">
        <div className="sidebars-sidebar">
          <div className="sidebars-sidebar-workspace">
            <img src="fixify.png" />
          </div>
        </div>
        <div className="sidebar">
          <h2 className="sidebar-content"> (R.string({j|Fixify ▾|j})) </h2>
          <p className="sidebar-content">
            (R.string({j|☆ AliceCryzstal|j}))
          </p>
          <div className="public-channels">
            <h4 className="sidebar-content"> (R.string("Channels")) </h4>
            (
              R.array(
                Belt.Array.map(publicChannels, channel =>
                  <div
                    onClick=((_) => self.send(OpenChannel(channel)))
                    className=(
                      " channel "
                      ++ (
                        self.state.currentChannel === channel ?
                          " channel-current " : ""
                      )
                    )
                    key=channel>
                    <span>
                      (
                        hasNextDialog(
                          channel,
                          self.state.newDialogue,
                          self.state.seen,
                        ) ?
                          R.string({j|✳️ |j}) : R.string({j| |j})
                      )
                    </span>
                    <span> (R.string("# " ++ channel)) </span>
                  </div>
                ),
              )
            )
          </div>
          <div className="private-channels">
            <h4 className="sidebar-content">
              (R.string("Direct Messages"))
            </h4>
            (
              R.array(
                Belt.Array.map(privateChannels, channel =>
                  <div
                    onClick=((_) => self.send(OpenChannel(channel)))
                    className=(
                      "channel "
                      ++ (
                        self.state.currentChannel === channel ?
                          " channel-current " : ""
                      )
                    )
                    key=channel>
                    <span>
                      (
                        hasNextDialog(
                          channel,
                          self.state.newDialogue,
                          self.state.seen,
                        ) ?
                          R.string({j|✳️ |j}) : R.string({j| |j})
                      )
                    </span>
                    <span> (R.string(" " ++ channel)) </span>
                  </div>
                ),
              )
            )
          </div>
        </div>
        <div className="chat-header">
          <h4> (R.string(self.state.currentChannel)) </h4>
        </div>
        <div className="chat-window">
          <div className="chat-history-container">
            <div className="chat-history">
              (
                R.array(
                  Belt.List.toArray(
                    self.state.dialogue
                    |. Belt.List.map(getChannelAndText)
                    |. Belt.List.keep(((channel, _)) =>
                         channel == self.state.currentChannel
                       )
                    |. Belt.List.mapWithIndex((i, (_, txt)) =>
                         <p key=(string_of_int(i))> (R.string(txt)) </p>
                       )
                    |. Belt.List.reverse,
                  ),
                )
              )
            </div>
          </div>
        </div>
        <div className="chat-options">
          (
            R.array(
              Belt.List.toArray(
                self.state.choices
                |. Belt.List.map(choice =>
                     (choice, getChannelAndText(choice.text))
                   )
                |. Belt.List.keep(((_, (channel, _))) =>
                     channel == self.state.currentChannel
                   )
                |. Belt.List.map(((choice, (_, text)) as a) => {
                     Js.log(a);
                     <div key=(string_of_int(choice.choicePoint))>
                       <button
                         className="choice"
                         href="#"
                         onClick=((_) => self.send(MakeChoice(choice)))>
                         (R.string(text))
                       </button>
                     </div>;
                   }),
              ),
            )
          )
        </div>
      </div>
    },
};